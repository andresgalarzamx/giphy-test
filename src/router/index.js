import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () =>
      import(/* webpackChunkName: "home" */ '@/views/HomeView.vue'),
  },
  {
    path: '/search/:query?',
    name: 'search',
    component: () =>
      import(/* webpackChunkName: "search" */ '@/views/SearchView.vue'),
  },
  {
    path: '/favorites',
    name: 'favorites',
    component: () =>
      import(/* webpackChunkName: "favorites" */ '@/views/FavoritesView.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
