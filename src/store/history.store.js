const state = {
  history: [],
}

const actions = {
  refreshHistory({ commit }) {
    const history = localStorage.getItem('history')
    let historyArray = []
    if (history) {
      historyArray = JSON.parse(history)
    }
    commit('setHistory', historyArray)
  },
  addPreviousSearch({ commit }, search) {
    const history = localStorage.getItem('history')
    let historyArray = []
    if (history) {
      historyArray = JSON.parse(history)
    }

    if (!historyArray.includes(search)) {
      historyArray.push(search)
    }
    localStorage.setItem('history', JSON.stringify(historyArray))
    commit('setHistory', historyArray)
  },
}

const mutations = {
  setHistory(state, payload) {
    state.history = payload
  },
}

const getters = {
  history: (state) => state.history,
}

const namespaced = true

export default {
  state,
  mutations,
  actions,
  namespaced,
  getters,
}
