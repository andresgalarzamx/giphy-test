const state = {
  favs: [],
}

const actions = {
  refreshFavs({ commit }) {
    const LsFavs = localStorage.getItem('favs')
    let favs = []
    if (LsFavs) {
      favs = JSON.parse(LsFavs)
    }
    commit('setFavs', favs)
  },
  toggleFav({ commit }, id) {
    const LsFavs = localStorage.getItem('favs')
    let favs = []
    if (LsFavs) {
      favs = JSON.parse(LsFavs)
    }

    console.log(favs)
    if (!favs.includes(id)) {
      favs.push(id)
    } else {
      favs.splice(favs.indexOf(id), 1)
    }
    localStorage.setItem('favs', JSON.stringify(favs))
    commit('setFavs', favs)
  },
}

const mutations = {
  setFavs(state, payload) {
    state.favs = payload
  },
}

const getters = {
  favs: (state) => state.favs,
  favsCount: (state) => state.favs.length,
  isFav: (state) => (id) => state.favs.includes(id),
}

const namespaced = true

export default {
  state,
  mutations,
  actions,
  namespaced,
  getters,
}
