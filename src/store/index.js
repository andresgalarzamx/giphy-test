import Vue from 'vue'
import Vuex from 'vuex'
import HistoryStore from '@/store/history.store'
import FavsStore from '@/store/fav.store'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    HistoryStore,
    FavsStore,
  },
})
