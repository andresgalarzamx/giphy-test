import axios from 'axios'

const api_key = process.env.VUE_APP_GIPHY_KEY

const giphyInstance = axios.create({
  baseURL: 'https://api.giphy.com/v1/gifs',
})

const giphy = {
  search: (search = 'perritos', params = { limit: 15 }) => {
    const query = search || 'perritos'
    return giphyInstance.get('search', {
      params: { q: query, ...params, api_key },
    })
  },
  trending: (params = { limit: 15 }) => {
    return giphyInstance.get('trending', {
      params: { api_key, ...params },
    })
  },
  gifs: (ids, params = { limit: 15 }) => {
    return giphyInstance.get('', {
      params: { api_key, ids: ids.join(',') },
      ...params,
    })
  },
}

export default giphy
